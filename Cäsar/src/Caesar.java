
public class Caesar implements Encryptor{

	
	
	private StringBuffer result;
	private String texten;
	private String textde;
	private int Scaesar;
	

	public Caesar(StringBuffer result, String texten, String textde, int scaesar) {
		super();
		
		this.texten = texten;
		this.textde = textde;
		Scaesar = scaesar;
	}

	public StringBuffer getResult() {
		return result;
	}

	public void setResult(StringBuffer result) {
		this.result = result;
	}

	public String getTexten() {
		return texten;
	}

	public void setTexten(String texten) {
		this.texten = texten;
	}

	public String getTextde() {
		return textde;
	}

	public void setTextde(String textde) {
		this.textde = textde;
	}
	public int getScaesar() {
		return Scaesar;
	}

	public void setScaesar(int scaesar) {
		Scaesar = scaesar;
	}

	
	
	
	public StringBuffer encrypton(String texten, int Scaesar) {
		
		result = new StringBuffer();
		for(int i=0; i<texten.length(); i++){
			if(Character.isUpperCase(texten.charAt(i))){
				char ch = (char)(((int)texten.charAt(i)) + Scaesar);
				result.append(ch);
			}
			else
			{
				char ch = (char)(((int)texten.charAt(i)) + Scaesar);
				result.append(ch);
			}
		}
		return result;
	}

	public StringBuffer decrypton(String textde, int Scaesar) {
		result = new StringBuffer();
		for(int i=0; i<textde.length(); i++){
			if(Character.isUpperCase(textde.charAt(i))){
				char ch = (char)(((int)textde.charAt(i)) - Scaesar);
				result.append(ch);
			}
			else
			{
				char ch = (char)(((int)textde.charAt(i)) - Scaesar);
				result.append(ch);
			}
		}
		return result;
		
	}

}
